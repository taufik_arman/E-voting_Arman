<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tbl_kandidat <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Kandidat <?php echo form_error('nama_kandidat') ?></label>
            <input type="text" class="form-control" name="nama_kandidat" id="nama_kandidat" placeholder="Nama Kandidat" value="<?php echo $nama_kandidat; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Visi <?php echo form_error('visi') ?></label>
            <input type="text" class="form-control" name="visi" id="visi" placeholder="Visi" value="<?php echo $visi; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Misi <?php echo form_error('misi') ?></label>
            <input type="text" class="form-control" name="misi" id="misi" placeholder="Misi" value="<?php echo $misi; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Thn Periode <?php echo form_error('thn_periode') ?></label>
            <input type="text" class="form-control" name="thn_periode" id="thn_periode" placeholder="Thn Periode" value="<?php echo $thn_periode; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tbl_kandidat') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>