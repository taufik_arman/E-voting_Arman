<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_warga extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_warga_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_warga/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_warga/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_warga/index.html';
            $config['first_url'] = base_url() . 'tbl_warga/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_warga_model->total_rows($q);
        $tbl_warga = $this->Tbl_warga_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_warga_data' => $tbl_warga,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('tbl_warga/tbl_warga_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_warga_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'id_NIK' => $row->id_NIK,
		'nama' => $row->nama,
		'status' => $row->status,
	    );
            $this->load->view('tbl_warga/tbl_warga_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_warga'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_warga/create_action'),
	    'id' => set_value('id'),
	    'id_NIK' => set_value('id_NIK'),
	    'nama' => set_value('nama'),
	    'status' => set_value('status'),
	);
        $this->load->view('tbl_warga/tbl_warga_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_NIK' => $this->input->post('id_NIK',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Tbl_warga_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_warga'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_warga_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_warga/update_action'),
		'id' => set_value('id', $row->id),
		'id_NIK' => set_value('id_NIK', $row->id_NIK),
		'nama' => set_value('nama', $row->nama),
		'status' => set_value('status', $row->status),
	    );
            $this->load->view('tbl_warga/tbl_warga_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_warga'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_NIK' => $this->input->post('id_NIK',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Tbl_warga_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_warga'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_warga_model->get_by_id($id);

        if ($row) {
            $this->Tbl_warga_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_warga'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_warga'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_NIK', 'id nik', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tbl_warga.php */
/* Location: ./application/controllers/Tbl_warga.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-13 21:53:47 */
/* http://harviacode.com */