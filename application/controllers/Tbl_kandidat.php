<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_kandidat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_kandidat_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_kandidat/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_kandidat/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_kandidat/index.html';
            $config['first_url'] = base_url() . 'tbl_kandidat/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_kandidat_model->total_rows($q);
        $tbl_kandidat = $this->Tbl_kandidat_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_kandidat_data' => $tbl_kandidat,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('tbl_kandidat/tbl_kandidat_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_kandidat_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nama_kandidat' => $row->nama_kandidat,
		'visi' => $row->visi,
		'misi' => $row->misi,
		'thn_periode' => $row->thn_periode,
	    );
            $this->load->view('tbl_kandidat/tbl_kandidat_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_kandidat'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_kandidat/create_action'),
	    'id' => set_value('id'),
	    'nama_kandidat' => set_value('nama_kandidat'),
	    'visi' => set_value('visi'),
	    'misi' => set_value('misi'),
	    'thn_periode' => set_value('thn_periode'),
	);
        $this->load->view('tbl_kandidat/tbl_kandidat_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_kandidat' => $this->input->post('nama_kandidat',TRUE),
		'visi' => $this->input->post('visi',TRUE),
		'misi' => $this->input->post('misi',TRUE),
		'thn_periode' => $this->input->post('thn_periode',TRUE),
	    );

            $this->Tbl_kandidat_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_kandidat'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_kandidat_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_kandidat/update_action'),
		'id' => set_value('id', $row->id),
		'nama_kandidat' => set_value('nama_kandidat', $row->nama_kandidat),
		'visi' => set_value('visi', $row->visi),
		'misi' => set_value('misi', $row->misi),
		'thn_periode' => set_value('thn_periode', $row->thn_periode),
	    );
            $this->load->view('tbl_kandidat/tbl_kandidat_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_kandidat'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nama_kandidat' => $this->input->post('nama_kandidat',TRUE),
		'visi' => $this->input->post('visi',TRUE),
		'misi' => $this->input->post('misi',TRUE),
		'thn_periode' => $this->input->post('thn_periode',TRUE),
	    );

            $this->Tbl_kandidat_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_kandidat'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_kandidat_model->get_by_id($id);

        if ($row) {
            $this->Tbl_kandidat_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_kandidat'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_kandidat'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_kandidat', 'nama kandidat', 'trim|required');
	$this->form_validation->set_rules('visi', 'visi', 'trim|required');
	$this->form_validation->set_rules('misi', 'misi', 'trim|required');
	$this->form_validation->set_rules('thn_periode', 'thn periode', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tbl_kandidat.php */
/* Location: ./application/controllers/Tbl_kandidat.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-13 21:51:11 */
/* http://harviacode.com */