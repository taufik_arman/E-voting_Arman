<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_kandidat extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(
                        array(
                                'id' => array(
                                        'type' => 'INT',
                                        'constraint' => 5,
                                        'unsigned' => TRUE,
                                        'auto_increment' => TRUE
                                ),
                                'nama_kandidat' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '100',
                                ),
                                'visi' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '100',
                                        'null' => FALSE,
                                ),
                                'misi' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '100',
                                        'null' => FALSE,
                                ),
								'thn_periode' => array(
                                        'type' => 'date',
                                        'null' => FALSE,
                                ),
                        ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('tbl_kandidat');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_kandidat');
        }
}