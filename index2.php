<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 ?>
<html>
<head>
 <title>E-Voting</title>
 <link rel="stylesheet" media="all" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css" type="text/css">
 <script src="<?php echo base_url()?>assets/bootstrap/css/bootstrap.js" type="text/javascript"></script>

 <script>
    $('.carousel').carousel({
        interval: 5000
    })
</script>
</head>

<!-- INI ADALAH HEADER -->
   <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">E-Voting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="">Home</a></li>
            <li><a href="index.php/tbl_kandidat">Kandidat</a></li>
			<li><a href="index.php/tbl_warga">Warga</a></li>
			<li><a href="index.php/tbl_voting">Voting</a></li>
            <li><a href="">Pesan</a></li>
   
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Login</a></li>
          </ul>
        </div>
      </div>
    </nav>
 <br><br><br><br><br>
 
 
 <!-- INI ADALAH TAMPILAN MENU UTAMA -->
    <div class="container">
      <div class="jumbotron">
        <h2>Pemilihan Calon Kepala Daerah Kelurahan Batununggal</h2><br>
	   <img src="assets/visi.jpg" class="img-responsive" alt="Responsive image">
      </div>

    </div>
	
	<div class="container">
      <div class="jumbotron">
        <h1>Statistik Hasil Voting Sementara</h1><br>
	   <img src="assets/statis.jpg" class="img-responsive" alt="Responsive image">
      </div>

    </div>
    <!-- Indicators -->

</div>
</body>
</html>